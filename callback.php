<?php
require_once('twitteroauth/twitteroauth.php');
require_once('config.php');

#$domain = '';
$domain = "100caloriechallenge.com";

if (isset($_REQUEST['oauth_token']) && $_COOKIE['oauth_token'] !== $_REQUEST['oauth_token']) {
  header('Location: ./clearsessions.php');
}

/* Create TwitteroAuth object with app key/secret and token key/secret from default phase */
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $_COOKIE['oauth_token'], $_COOKIE['oauth_token_secret']);

/* Request access tokens from twitter */
$access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);
$oauth_token = $access_token['oauth_token'];
$oauth_token_secret = $access_token['oauth_token_secret'];

/* Save the access tokens. Normally these would be saved in a database for future use. */
/* Remove no longer needed request tokens */
setcookie('oauth_token', $oauth_token, 0, '', $domain);
setcookie('oauth_token_secret', $oauth_token_secret, 0, '', $domain);

header('Location: ./index.php');
?>