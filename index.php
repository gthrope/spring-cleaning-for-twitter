<?php
require_once('twitteroauth/twitteroauth.php');
require_once('config.php');

$oauth_token = $_COOKIE['oauth_token'];
$oauth_token_secret = $_COOKIE['oauth_token_secret'];

if (empty($oauth_token) || empty($oauth_token_secret)) {
    header('Location: ./clearsessions.php');
}

/* Create a TwitterOauth object with consumer/user tokens. */
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $oauth_token, $oauth_token_secret);

$friends = $connection->get('friends/ids');

$tweetedNever = array();
$tweeted365 = array();
$tweeted90 = array();
$tweeted30 = array();
$tweeted7 = array();

$ids = $friends->ids;
$numIds = count($ids);
$batchSize = 100;
$now = time();
for ($i = 0; $i <= $numIds; $i += $batchSize)
{
	$userIds = array_slice($ids, $i, $batchSize);
	$userIdsCommaSeparated = implode(",", $userIds);
	$parameters = array('user_id' => $userIdsCommaSeparated);
	
	$newUsers = $connection->get("users/lookup", $parameters);

	foreach($newUsers as $newUser)
	{
#		$lastTweet = '';
		$lastTweetTime = 0;
		if($newUser->status)
		{
#			$lastTweet = $newUser->status->text;
			$lastTweetTime = strtotime($newUser->status->created_at);
		}
		else if($newUser->statuses_count > 0)
		{
			// weird edge case
			continue;
		}

		$trimmedName = trim($newUser->name);
		$trimmedScreenName = trim($newUser->screen_name);
		$key = strtoupper($trimmedName . $trimmedScreenName);
		$user = array(
			"id" => $newUser->id,
			"name" => $trimmedName,
			"imgUrl" => $newUser->profile_image_url,
			"screenName" => $trimmedScreenName
#			"lastTweet" => $newUser->status->text,
#			"lastTweetTime" => date("M j, Y", $lastTweetTime)
		);
		
		if($lastTweetTime == 0)
		{
			$tweetedNever[$key] = $user;
		}
		else
		{
			$daysPast = round(($now - $lastTweetTime)/86400); // 60*60*24
			
			if($daysPast > 365)
			{
				$tweeted365[$key] = $user;
			}
			else if($daysPast > 90)
			{
				$tweeted90[$key] = $user;
			}
			else if($daysPast > 30)
			{
				$tweeted30[$key] = $user;
			}
			else if($daysPast > 7)
			{
				$tweeted7[$key] = $user;
			}
		}
	}
}

ksort($tweetedNever);
ksort($tweeted365);
ksort($tweeted90);
ksort($tweeted30);
ksort($tweeted7);

?>

<!DOCTYPE HTML>
<html>
<head>
	<title>Spring Cleaning for Twitter</title>
	<link rel="stylesheet" type="text/css" href="css/common.css" />
	<script src="http://code.jquery.com/jquery.js" type="text/javascript"></script>
	<script src="js/jsrender.js" type="text/javascript"></script>
</head>

<body>
<form id="main" method="POST" action="unfollow.php">
<h1>Spring Cleaning for Twitter</h1>
<input style="height:100px;width:200px;" type="submit" value="Remove selected followers" />
<br/><br/>

<div style="text-align:left;">

<br/><br/>
People you follow who have never tweeted:
<br/><br/>
<div id="tweetedNever"><div class="nobody">None!</div></div>
<br style="clear:both;"/><br/>

<br/><br/>
People you follow who haven't tweeted in the past year:
<br/><br/>
<div id="tweeted365"><div class="nobody">None!</div></div>
<br style="clear:both;"/><br/>

<br/><br/>
People you follow who haven't tweeted in the past 90 days:
<br/><br/>
<div id="tweeted90"><div class="nobody">None!</div></div>
<br style="clear:both;"/><br/>

<br/><br/>
People you follow who haven't tweeted in the past 30 days:
<br/><br/>
<div id="tweeted30"><div class="nobody">None!</div></div>
<br style="clear:both;"/><br/>

<br/><br/>
People you follow who haven't tweeted in the past week:
<br/><br/>
<div id="tweeted7"><div class="nobody">None!</div></div>
<br style="clear:both;"/><br/>

</div>

</form>

<script id="followTemplate" type="text/x-jsrender" data-jsv-tmpl="_0">
	<div class="follow">
		<input type="checkbox" class="checkbox" name="unfollows[]" value="{{:id}}" />
		<img class="icon" src="{{:imgUrl}}"/>
		<div class="info">
			<a href="https://twitter.com/#!/{{:screenName}}" target="_blank">{{:name}}</a>
		</div>
	</div>
</script>

<script type="text/javascript">

	var tweetedNever = <?php echo json_encode(array_values($tweetedNever)) ?>;
	var tweeted365 = <?php echo json_encode(array_values($tweeted365)) ?>;
	var tweeted90 = <?php echo json_encode(array_values($tweeted90)) ?>;
	var tweeted30 = <?php echo json_encode(array_values($tweeted30)) ?>;
	var tweeted7 = <?php echo json_encode(array_values($tweeted7)) ?>;
	
	if(tweetedNever.length > 0)
	{
		$("#tweetedNever").html(
			$("#followTemplate").render(tweetedNever)
		);
	}
	if(tweeted365.length > 0)
	{
		$("#tweeted365").html(
			$("#followTemplate").render(tweeted365)
		);
	}
	
	if(tweeted90.length > 0)
	{
		$("#tweeted90").html(
			$("#followTemplate").render(tweeted90)
		);
	}
	
	if(tweeted30.length > 0)
	{
		$("#tweeted30").html(
			$("#followTemplate").render(tweeted30)
		);
	}
	
	if(tweeted7.length > 0)
	{
		$("#tweeted7").html(
			$("#followTemplate").render(tweeted7)
		);
	}

</script>

</body>
</html>
