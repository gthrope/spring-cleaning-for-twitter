<?php
require_once('twitteroauth/twitteroauth.php');
require_once('config.php');

$oauth_token = $_COOKIE['oauth_token'];
$oauth_token_secret = $_COOKIE['oauth_token_secret'];
$unfollows = $_POST["unfollows"];

if (empty($oauth_token) || empty($oauth_token_secret)) {
    header('Location: ./clearsessions.php');
}

# weird way of handling case where user doesn't check anything
if (empty($unfollows)) {
	header('Location: ./index.php');
}

/* Create a TwitterOauth object with consumer/user tokens. */
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $oauth_token, $oauth_token_secret);

foreach($unfollows as $unfollow)
{
	$parameters = array('user_id' => $unfollow);
	$connection->post('friendships/destroy', $parameters);	// note that this returns the unfollowed user
}

?>

<!DOCTYPE HTML>
<html>
<head>
	<title>Spring Cleaning for Twitter</title>
	<link rel="stylesheet" type="text/css" href="css/common.css" />
</head>
<body>
<h1>You are no longer following the <?php echo ((count($unfollows) > 1) ? (count($unfollows) . " users") : "user") ?> you selected!</h1>
<a href="index.php">Return home</a>
</body>
</html>
