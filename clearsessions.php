<?php
/**
 * @file
 * Clears cookies and redirects to the login page.
 */

#$domain = '';
$domain = "100caloriechallenge.com";

/* Clear cookies */
setcookie ("oauth_token", "", time() - 3600, '', $domain);
setcookie ("oauth_token_secret", "", time() - 3600, '', $domain);
 
/* Redirect to page with the connect to Twitter option. */
header('Location: ./login.php');
