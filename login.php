<?php
require_once('twitteroauth/twitteroauth.php');
require_once('config.php');

#$domain = '';
$domain = "100caloriechallenge.com";

/* Build TwitterOAuth object with client credentials. */
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
/* handle case where $connection->http_code != 200 */

/* Get temporary credentials. */
$request_token = $connection->getRequestToken(OAUTH_CALLBACK);
$token = $request_token['oauth_token'];
$url = $connection->getAuthorizeURL($token);

setcookie('oauth_token', $token, 0, '', $domain);
setcookie('oauth_token_secret', $request_token['oauth_token_secret'], 0, '', $domain);
?>

<html>
  <head>
	<title>Spring Cleaning for Twitter</title>
	<link rel="stylesheet" type="text/css" href="css/common.css" />
  </head>
  <body>
	<div id="main">
	<br/><br/><br/><br/>
	<h1>Spring Cleaning for Twitter</h1>
	<br/>
	A tool to help you stop following people who never tweet!
	<br/><br/><br/>
	<a href="<?php echo $url?>"><img src="img/lighter.png" alt="Sign in with Twitter"/></a>
	</div>
  </body>
</html>